const txtFirstName = document.querySelector("#txt-firstName");
const txtLastName = document.querySelector("#txt-lastName");
const spanFullName = document.querySelector("#span-full-name");

txtFirstName.addEventListener("keyup", getFullName);
txtLastName.addEventListener("keyup", getFullName);

function getFullName() {
    spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
}